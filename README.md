# Echoplex Looper V1

This looper is based on the Oberheim Echoplex Looper. The objective of this project is to create a fully fonctionnal multi-syncronised looper.

## What's a looper

It's a software devices used to create and modify music loops (a repeating section of sound material).

## How do I use it?

This is a simple multi-syncronised looper based on the Oberheim Echolpex Looper.

- [Demo showing a introduction to the Oberheim Echoplex Looper](https://www.youtube.com/watch?v=yKqoJxgn8bc)
- [Demo showing our digitalised version](https://youtu.be/O8wWIFb_z7E)

All the loops are saved in the document folder of the current user.

The wiki can be find [here](https://gitlab.com/DamienAuvray/echoplex-looper/wikis/home).

## System Requirements
#### Building JUCE Projects
- __macOS__: macOS 10.11 and Xcode 7.3.1
- __Windows__: Windows 8.1 and Visual Studio 2013 64-bit
- __Linux__: GCC 5.0

#### Minimum Deployment Targets
- __macOS__: macOS 10.7
- __Windows__: Windows Vista
- __Linux__: Mainstream Linux distributions

## How did you create this?

The looper was written using Juce, a cross-platform C++ library inspired by the Java JDK. Among other things, Juce provides a GUI for generating boilerplate for audio plugins.

The code we wrote is essentially a emulated Echoplex Looper and providing a UI.

## Building

So far I've only built under Windows and linux. There is also an OSX build, but I currently have no way to build it myself on OSX.

### Windows Build Instructions

1. Download Juce (http://www.juce.com/)
3. Run "The Projucer" executable included in Juce.
4. Open Echoplex.jucer
5. Hit "Save Project and Open in Visual Studio". I use Visual Studio Entreprise 2017.
6. Select the build: "Release - 64-bit" and set platform to x64.
8. Build!

### Linux Build Instructions

1. Download Juce (http://www.juce.com)
3. Open Echoplex.jucer in the Introjucer application.
   - Right click on the Echoplex label in the file tree.
   - Select "Create Linux Makefile Target".
4. Make. Output at Builds/LinuxMakefile/build/Echoplex.so

## Contributing
For bug reports and features requests, please visit the [JUCE Forum](https://forum.juce.com/) - 
the JUCE developers are active there and will read every post and respond accordingly.
Or just add a Issues to the gitlab project.

## License
The core JUCE modules (juce_audio_basics, juce_audio_devices, juce_blocks_basics, juce_core 
and juce_events) are permissively licensed under the terms of the 
[ISC license](http://www.isc.org/downloads/software-support-policy/isc-license/). 
Other modules are covered by a 
[GPL/Commercial license](https://www.gnu.org/licenses/gpl-3.0.en.html).

There are multiple commercial licensing tiers for JUCE 5, with different terms for each:
- JUCE Personal (developers or startup businesses with revenue under 50K USD) - free
- JUCE Indie (small businesses with revenue under 200K USD) - $35/month
- JUCE Pro (no revenue limit) - $65/month
- JUCE Eductational (no revenue limit) - free for bona fide educational institutes