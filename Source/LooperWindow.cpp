/*
  ==============================================================================

    LooperWindow.cpp
    Created: 3 Jan 2019 3:09:31pm
    Author:  nicolas & damien

  ==============================================================================
*/

#include "LooperWindow.h"
#include "MainComponent.h"

//==============================================================================

/**
* Builder
*/
LooperWindow::LooperWindow() : DocumentWindow("Looper",
	Desktop::getInstance().getDefaultLookAndFeel()
	.findColour(ResizableWindow::backgroundColourId),
	DocumentWindow::closeButton)
{
	setUsingNativeTitleBar(true);
	RCcontent = new RecorderComponent();
	setContentOwned(RCcontent, true);

	centreWithSize(650, 450);
	setVisible(true);
}

/**
* Button listener
*/
void LooperWindow::closeButtonPressed()
{
	int value = Desktop::getInstance().getNumComponents();
	//DBG(Desktop::getInstance().getComponent(0)->getName());
	//DBG(Desktop::getInstance().getComponent(1)->getName());
	MainComponent * MC = nullptr;
	for (int i = 0; i < value;i++) {
		if (Desktop::getInstance().getComponent(i)->getName().compare("Echoplex")==0) {
			//DBG(Desktop::getInstance().getComponent(i)->getName());
			MC = (MainComponent *)Desktop::getInstance().getComponent(i)->getChildComponent(0);
		}
	}

	MC->timerWindowClose();
	RCcontent->~RecorderComponent();
	setVisible(false);
}

/**
* Delete the LooperWindow
*/
void LooperWindow::deleteWindow()
{
	delete this;
}

/**
* Delete the LooperWindow
* return RecorderComponent
*/
RecorderComponent * LooperWindow::getRCcontent()
{
	return RCcontent;
}
