/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"

//==============================================================================

/**
* Builder
*/
MainComponent::MainComponent()
{
	setLookAndFeel(nullptr);
	lookAndFeel = new LookAndFeel_V4(LookAndFeel_V4::getGreyColourScheme());
	setLookAndFeel(lookAndFeel);
	
	txt_timeout.reset(new Label("txt_timeout", TRANS("Timeloop : 0")));
	addAndMakeVisible(txt_timeout.get());
	txt_timeout->setEditable(false, false, false);
	txt_timeout->setColour(TextEditor::textColourId, Colours::black);
	txt_timeout->setBounds(60, 50, 85, 50);

	btn_open.reset(new TextButton("btn_open"));
	addAndMakeVisible(btn_open.get());
	btn_open->setButtonText(TRANS("CLICK ME"));
	btn_open->addListener(this);
	btn_open->setBounds(50, 10, 100, 50);

	timeout = RelativeTime(0);
	numberActiveWindow = 0;

	setSize(200, 100);

}

/**
* Destructor
*/
MainComponent::~MainComponent()
{

	setLookAndFeel(nullptr);
	//closing everyTimers opened
	for (int i = 0; i < timerWindowList.size(); i++) {
		LooperWindow * tw = timerWindowList.getUnchecked(i);
		tw->deleteWindow();
	}
	btn_open = nullptr;
	txt_timeout = nullptr;
}

//==============================================================================
void MainComponent::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));

    g.setFont (Font (16.0f));
    g.setColour (Colours::white);
}

void MainComponent::resized()
{
	// window not resizable
}

/**
* Button listener
*/
void MainComponent::buttonClicked(Button* buttonThatWasClicked)
{
	if (buttonThatWasClicked == btn_open.get())
	{
		if (timeout == RelativeTime(0)) {
			

			if (numberActiveWindow == 0) {
				for (int i = 0; i < timerWindowList.size(); i++) {
					LooperWindow * tw = timerWindowList.getUnchecked(i);
					tw->deleteWindow();
				}
				timerWindowList.clearQuick();
			}
			LooperWindow * tw = new LooperWindow();
			timerWindowList.add(tw);

			numberActiveWindow++;



			DBG("open btn clicked : " + String(timerWindowList.size()) + " Timer(s) opened, " + String(numberActiveWindow) + " Timer(s) actived, ");
		}
		else {
			DBG("Cant open new timer if timeout is defined");
		}
	}
}

/**
* Set the TimeLoop and share it to all the loopers.
*/
void MainComponent::setTimeout(RelativeTime RT)
{
	timeout = RT;
	txt_timeout->setText("Timeloop : " + String(timeout.inSeconds()),NotificationType::sendNotification);
	txt_timeout->setBounds(57, 50, 85, 50);
	
	for (int i = 0; i < timerWindowList.size(); i++) {
		LooperWindow * tw = timerWindowList.getUnchecked(i);
		tw->getRCcontent()->getMyTimer()->setTimeout(timeout);
	}
}

/**
* Called when we close looper window
* Check if the number of active windows is equal to 0 and if it is, reset the Timeloop.
*/
void MainComponent::timerWindowClose()
{

	numberActiveWindow--;
	DBG("active timerwindows : " + String(numberActiveWindow));

	if (numberActiveWindow == 0) {
		lock.enterWrite();

		setTimeout(RelativeTime(0));

		lock.exitWrite();
	}
}

/**
* RelativeTime getTimeout()
* return the timeout
*/
RelativeTime MainComponent::getTimeout()
{
	RelativeTime tmp = timeout;

	return tmp;

}




//MainComponent * MC = (MainComponent*) Desktop::getInstance().getComponent(0)->getChildComponent(0);