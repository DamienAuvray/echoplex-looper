/*
  ==============================================================================

	MyTimer.h
	Created: 3 Jan 2019 3:09:31pm
	Author:  nicolas & damien

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

class MyTimer
{
public:
	//==============================================================================
	MyTimer();
	~MyTimer();

	//==============================================================================
	void startAndStop();
	bool isStarted();
	RelativeTime getTime();
	void setTimeout(RelativeTime relativeTime);
	RelativeTime getTimeout();

private:
	//==============================================================================
	// Your private member variables go here...
	Time startTime;
	RelativeTime interval;
	RelativeTime timeout;
	bool started;

	void updateTime();

};
