/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "LooperWindow.h"

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component, public Button::Listener
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;

	void buttonClicked(Button * buttonThatWasClicked);

	void setTimeout(RelativeTime RT);
	void timerWindowClose();
	RelativeTime getTimeout();

private:
    //==============================================================================
    // Your private member variables go here...

	ScopedPointer<LookAndFeel> lookAndFeel;

	std::unique_ptr<TextButton> btn_open;
	std::unique_ptr<Label> txt_timeout;

	Array<LooperWindow * > timerWindowList;
	int numberActiveWindow;

	RelativeTime timeout;

	ReadWriteLock lock;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};
