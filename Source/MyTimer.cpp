/*
  ==============================================================================

	MyTimer.cpp
	Created: 3 Jan 2019 3:09:31pm
	Author:  nicolas & damien

  ==============================================================================
*/

#include "MyTimer.h"
#include "MainComponent.h"

/**
* Builder
*/
MyTimer::MyTimer()
{
	startTime = Time::getCurrentTime();
	interval = startTime - Time::getCurrentTime();
	started = false;
	timeout = RelativeTime(0);
}

/**
* Destructor
*/
MyTimer::~MyTimer()
{
}

/**
* start and stop the timer
*/
void MyTimer::startAndStop()
{
	int value = Desktop::getInstance().getNumComponents();
	MainComponent * MC = __nullptr;
	for (int i = 0; i < value; i++) {
		if (Desktop::getInstance().getComponent(i)->getName().compare("Echoplex") == 0) {
			MC = (MainComponent *)Desktop::getInstance().getComponent(i)->getChildComponent(0);
		}
	}
	bool timeoutDefined = false;
	if (MC->getTimeout() > RelativeTime(0)) {
		timeout = MC->getTimeout();
		timeoutDefined = true;
		started = false;
	}
	if (started) {
		interval = Time::getCurrentTime() - startTime;
		timeout = interval;
		MC->setTimeout(this->getTimeout());
		started = false;
	}
	else {
		startTime = Time::getCurrentTime();
		if (!timeoutDefined)
			started = true;
	}
}

/**
* return true if the timer is started
*/
bool MyTimer::isStarted()
{
	return started;
}

/**
* Return the interval between startTime and now if timer is started
* return last interval registered othewise
*/
RelativeTime MyTimer::getTime()
{
	updateTime();
	return interval;
}

/**
* Return the timeout
*/
RelativeTime MyTimer::getTimeout()
{
	return timeout;
}

/**
* Register manually an interval
* works only if timer is not started
*/
void MyTimer::setTimeout(RelativeTime relativeTime)
{
	if (!started)
		timeout = relativeTime;
}

/**
* if timer is started update the registered interval
*/
void MyTimer::updateTime()
{
	/* DEBUGGAGE */
	/*DBG(" timeout : " + String(timeout.inSeconds()));
	DBG(" interval : " + String(interval.inSeconds()));
	DBG(" starttime : " + String(startTime.toString(false, true, true, true)));*/
	if (started)
		interval = Time::getCurrentTime() - startTime;
	else {
		if (timeout > RelativeTime(0)) {
			interval = Time::getCurrentTime() - startTime;
			while (timeout > RelativeTime(0) && interval >= timeout)
				interval -= timeout;
		}
	}

}