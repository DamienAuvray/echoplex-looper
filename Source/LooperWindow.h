/*
  ==============================================================================

	LooperWindow.h
	Created: 3 Jan 2019 3:09:31pm
	Author:  nicolas & damien

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "AudioRecorder.h"


/**
* LooperWindow class
* Simple class that create a RecorderComponent
*/
class LooperWindow : public DocumentWindow
{
public:
	//==============================================================================
	LooperWindow();

	//==============================================================================
	void closeButtonPressed() override;
	void deleteWindow();
	RecorderComponent * getRCcontent();
private:
	//==============================================================================
	// Your private member variables go here...
	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(LooperWindow) 
	RecorderComponent * RCcontent;
};
