/*
  ==============================================================================

	AudioRecorder.h
	Created: 3 Jan 2019 3:09:31pm
	Author:  nicolas & damien

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "MyTimer.h"

//==============================================================================
/** A simple class that acts as an AudioIODeviceCallback and writes the
	incoming audio data to a WAV file.
*/
class AudioRecorder : public AudioIODeviceCallback
{
public:
	/**
	* Builder
	*/
	AudioRecorder()
	{
		backgroundThread.startThread();
	}

	/**
	* Destructor
	*/
	~AudioRecorder()
	{
		stop();
	}

	//==============================================================================

	/**
	* Start the recording
	*/
	void startRecording(const File& file)
	{
		stop();

		if (sampleRate > 0)
		{
			// Create an OutputStream to write to our destination file...
			file.deleteFile();

			if (auto fileStream = std::unique_ptr<FileOutputStream>(file.createOutputStream()))
			{
				// Now create a WAV writer object that writes to our output stream...
				WavAudioFormat wavFormat;

				if (auto writer = wavFormat.createWriterFor(fileStream.get(), sampleRate, 1, 16, {}, 0))
				{
					fileStream.release(); // (passes responsibility for deleting the stream to the writer object that is now using it)

					// Now we'll create one of these helper objects which will act as a FIFO buffer, and will
					// write the data to disk on our background thread.
					threadedWriter.reset(new AudioFormatWriter::ThreadedWriter(writer, backgroundThread, 32768));

					// Reset our recording thumbnail
					nextSampleNum = 0;

					// And now, swap over our active writer pointer so that the audio callback will start using it..
					const ScopedLock sl(writerLock);
					activeWriter = threadedWriter.get();
				}
			}
		}
	}

	/**
	* Stop the recording
	*/
	void stop()
	{
		// First, clear this pointer to stop the audio callback from using our writer object..
		{
			const ScopedLock sl(writerLock);
			activeWriter = nullptr;
		}

		// Now we can delete the writer object. It's done in this order because the deletion could
		// take a little time while remaining data gets flushed to disk, so it's best to avoid blocking
		// the audio callback while this happens.
		threadedWriter.reset();
	}

	/**
	* Check if the looper is recording
	* @return boolean, true if is recording
	*/
	bool isRecording() const
	{
		return activeWriter.load() != nullptr;
	}

	//==============================================================================
	/**
	* Set the sampleRate
	*/
	void audioDeviceAboutToStart(AudioIODevice* device) override
	{
		sampleRate = device->getCurrentSampleRate();
	}

	/**
	* Init the sampleRate
	*/
	void audioDeviceStopped() override
	{
		sampleRate = 0;
	}

	/**
	* Set the audioDevice
	*/
	void audioDeviceIOCallback(const float** inputChannelData, int numInputChannels,
		float** outputChannelData, int numOutputChannels,
		int numSamples) override
	{
		const ScopedLock sl(writerLock);
		if (activeWriter.load() != nullptr)
		{
			activeWriter.load()->write(inputChannelData, numSamples);

			// Create an AudioBuffer to wrap our incoming data, note that this does no allocations or copies, it simply references our input data
			nextSampleNum += numSamples;
		}

		// We need to clear the output buffers, in case they're full of junk..
		for (int i = 0; i < numOutputChannels; ++i)
			if (outputChannelData[i] != nullptr)
				FloatVectorOperations::clear(outputChannelData[i], numSamples);

		numInputChannels;
	}

private:
	TimeSliceThread backgroundThread{ "Audio Recorder Thread" }; // the thread that will write our audio data to disk
	std::unique_ptr<AudioFormatWriter::ThreadedWriter> threadedWriter; // the FIFO used to buffer the incoming data
	double sampleRate = 0.0;
	int64 nextSampleNum = 0;

	CriticalSection writerLock;
	std::atomic<AudioFormatWriter::ThreadedWriter*> activeWriter{ nullptr };
};


//==============================================================================
/*
	This component lives inside our window, and this is where you should put all
	your controls and content.
*/
class RecorderComponent : public Component,
                      private AudioIODeviceCallback
{
public:
    //==============================================================================

	/**
	* Builder
	*/
    RecorderComponent()
        : audioSetupComp (audioDeviceManager, 0, 256, 0, 256,
                          false, false, false, false)
    {
		//---------- Graphics
		setLookAndFeel(nullptr);
		lookAndFeel = new LookAndFeel_V4(LookAndFeel_V4::getGreyColourScheme());
		setLookAndFeel(lookAndFeel);

        audioDeviceManager.initialise (2, 2, nullptr, true, {}, nullptr);
        audioDeviceManager.addAudioCallback (this);

        addAndMakeVisible (audioSetupComp);

		addAndMakeVisible(explanationLabel);
		explanationLabel.setFont(Font(15.0f, Font::plain));
		explanationLabel.setJustificationType(Justification::topLeft);
		explanationLabel.setEditable(false, false, false);
		explanationLabel.setColour(TextEditor::textColourId, Colours::black);
		explanationLabel.setColour(TextEditor::backgroundColourId, Colour(0x00000000));

		addAndMakeVisible(recordButton);
		recordButton.setColour(TextButton::buttonColourId, Colour(0xffff5c5c));
		recordButton.setColour(TextButton::textColourOnId, Colours::black);

		//---------- Audio
		recordButton.onClick = [this]
		{
			if (auto* device = audioDeviceManager.getCurrentAudioDevice()) {
				if (recorder.isRecording()) {
					if (myTimer.getTimeout() != (RelativeTime)0) {
						do {
							Time::waitForMillisecondCounter(Time::getMillisecondCounter() + 5);
						} while (myTimer.getTime() >= (RelativeTime)0.015);
						stopRecording();
					}
					else {
						myTimer.startAndStop();
						myTimer.setTimeout(myTimer.getTime());
						stopRecording();
					}
				}
				else {
					myTimer.startAndStop();
					startRecording();
				}
			}
		};
		audioDeviceManager.addAudioCallback(&recorder);
		
		setSize(650, 450);

		//-------------- LOOP
		audioDeviceManager.addAudioCallback(&audioSourcePlayer);
		audioSourcePlayer.setSource(&transportSource);

		formatManager.registerBasicFormats();
		thread.startThread();
    }

	/**
	* Destructor
	*/
    ~RecorderComponent()
    {
		//------------- Graphics
		setLookAndFeel(nullptr);

		//------------- Audio
		audioDeviceManager.removeAudioCallback(&recorder);

		//------------- loop
		transportSource.setSource(nullptr);
		audioSourcePlayer.setSource(nullptr);

		audioDeviceManager.removeAudioCallback(&audioSourcePlayer);

		//------------- sync
		myTimer.~MyTimer();
    }

	//==============================================================================
	void paint(Graphics& g) override
	{
		g.fillAll(getLookAndFeel().findColour(ResizableWindow::backgroundColourId));
	}

    //==============================================================================
    void resized() override
    {
		auto area = getLocalBounds();

		recordButton.setBounds(area.removeFromTop(36).reduced(8));
		explanationLabel.setBounds(area.removeFromTop(100).reduced(8));

		audioSetupComp.setBounds(area.removeFromTop(audioSetupComp.getHeight()));
    }

    //==============================================================================
    void audioDeviceIOCallback (const float** /*inputChannelData*/, int /*numInputChannels*/,
                                float** outputChannelData, int numOutputChannels,
                                int numSamples) override
    {
        // make buffer
        AudioBuffer<float> buffer (outputChannelData, numOutputChannels, numSamples);

        // clear it to silence
        buffer.clear();
    }

    void audioDeviceAboutToStart (AudioIODevice* ) override    { }

    void audioDeviceStopped() override {}

	//============================================================================== Sync

	/**
	* getMyTimer
	* return MyTimer *
	*/
	MyTimer * getMyTimer()
	{
		return &myTimer;
	}

private:
    //==============================================================================
	//-------------------------- graphics
	ScopedPointer<LookAndFeel> lookAndFeel;
    //==============================================================================
	//-------------------------- Audio
    AudioDeviceManager audioDeviceManager; 
    AudioDeviceSelectorComponent audioSetupComp;
	//-------------------------- loop
	AudioSourcePlayer audioSourcePlayer;
	AudioTransportSource transportSource;
	std::unique_ptr<AudioFormatReaderSource> currentAudioFileSource;

	AudioFormatManager formatManager;
	TimeSliceThread thread{ "audio file preview" };
	//-------------------------- sync
	MyTimer myTimer;

	//--
	AudioRecorder recorder{ };

	Label explanationLabel{ {}, "This page demonstrates how to record a wave file from the live audio input..\n\n"
								 #if (JUCE_ANDROID || JUCE_IOS)
								  "After you are done with your recording you can share with other apps."
								 #else
								  "Pressing record will start recording a file in your \"Documents\" folder."
								 #endif
	};
	TextButton recordButton{ "Record" };
	File lastRecording;

	/**
	* Start recording
	*/
	void startRecording()
	{
		//stop playing
		transportSource.stop();

		if (!RuntimePermissions::isGranted(RuntimePermissions::writeExternalStorage))
		{
			SafePointer<RecorderComponent> safeThis(this);

			RuntimePermissions::request(RuntimePermissions::writeExternalStorage,
				[safeThis](bool granted) mutable
			{
				if (granted)
					safeThis->startRecording();
			});
			return;
		}

#if (JUCE_ANDROID || JUCE_IOS)
		auto parentDir = File::getSpecialLocation(File::tempDirectory);
#else
		auto parentDir = File::getSpecialLocation(File::userDocumentsDirectory);
#endif

		lastRecording = parentDir.getNonexistentChildFile("JUCE Demo Audio Recording", ".wav");

		recorder.startRecording(lastRecording);

		recordButton.setButtonText("Stop");

	}

	/**
	* Stop recording
	*/
	void stopRecording()
	{
		recorder.stop();
		
		//=================================== loop
		// unload the previous file source and delete it..
		transportSource.stop();
		transportSource.setSource(nullptr);
		currentAudioFileSource.reset();


		AudioFormatReader* reader = nullptr;
		DBG(lastRecording.getFileName());
		reader = formatManager.createReaderFor(lastRecording);
		AudioFormatReaderSource * tmp = new AudioFormatReaderSource(reader, true);
		tmp->setLooping(true);
		currentAudioFileSource.reset(tmp);

		transportSource.setSource(currentAudioFileSource.get(),
			32768,                   // tells it to buffer this many samples ahead
			&thread,                 // this is the background thread to use for reading-ahead
			reader->sampleRate);

		transportSource.setLooping(true);
		transportSource.start();
		//=================================== End loop

#if (JUCE_ANDROID || JUCE_IOS)
		SafePointer<AudioRecordingDemo> safeThis(this);
		File fileToShare = lastRecording;

		ContentSharer::getInstance()->shareFiles(Array<URL>({ URL(fileToShare) }),
			[safeThis, fileToShare](bool success, const String& error)
		{
			if (fileToShare.existsAsFile())
				fileToShare.deleteFile();

			if (!success && error.isNotEmpty())
			{
				NativeMessageBox::showMessageBoxAsync(AlertWindow::WarningIcon,
					"Sharing Error",
					error);
			}
		});
#endif

		lastRecording = File();
		recordButton.setButtonText("Record");
	}


	//--------------------------

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(RecorderComponent)
};
